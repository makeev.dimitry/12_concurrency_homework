#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import gzip
import sys
import glob
import logging
import collections
import time
from functools import partial, wraps
from multiprocessing import cpu_count, Pool, Value
from optparse import OptionParser
from queue import Queue
from threading import Thread

import memcache
import appsinstalled_pb2

RETRY_COUNT = 3
RETRY_DELAY = 0.2
SOCKET_TIMEOUT = 3
NORMAL_ERR_RATE = 0.01
AppsInstalled = collections.namedtuple("AppsInstalled",
                                       ["dev_type", "dev_id", "lat", "lon", "apps"])


def retry(_count=3, _delay=0.2):
    """Декоратор повторного вызова метода при исключении"""
    def func_wrapper(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            count = _count
            while count > 0:
                try:
                    return func(*args, **kwargs)
                except Exception:
                    time.sleep(_delay)
                count -= 1
        return wrapper
    return func_wrapper


class MemcacheThread(Thread):
    def __init__(self, addr: str, queue: Queue, processed: Value, errors: Value, config):
        Thread.__init__(self)

        self.queue = queue
        self.addr = addr
        self.client = memcache.Client([addr], socket_timeout=SOCKET_TIMEOUT)
        self.config = config

        self.total_processed = processed
        self.total_errors = errors
        self.thread_processed = 0
        self.thread_errors = 0

    def run(self):
        while True:
            data = self.queue.get()
            if data == 'quit':
                break

            self.insert_apps_installed(data)

            with self.total_processed.get_lock():
                self.total_processed.value += self.thread_processed
                self.thread_processed = 0

            with self.total_errors.get_lock():
                self.total_errors.value += self.thread_errors
                self.thread_errors = 0

            self.queue.task_done()

    def __parse_apps_installed_data(self, apps_installed_data):
        result = {}
        for data in apps_installed_data:
            ua = appsinstalled_pb2.UserApps()
            ua.lat = data.lat
            ua.lon = data.lon
            key = "%s:%s" % (data.dev_type.decode(), data.dev_id.decode())
            ua.apps.extend(data.apps)

            result[key] = ua.SerializeToString()

            if self.config.dry:
                logging.debug("%s - %s -> %s" % (
                    self.addr,
                    key,
                    str(ua).replace("\n", " ")
                ))
        return result

    @retry(_count=RETRY_COUNT, _delay=RETRY_DELAY)
    def insert_apps_installed(self, data):
        try:
            parsed = self.__parse_apps_installed_data(data)
            if not self.config.dry:
                self.client.set_multi(parsed)
            self.thread_processed += len(data)
        except Exception as exc:
            logging.exception("Cannot write to memcache %s: %s" % (self.addr, exc))
            self.thread_errors += len(data)


def dot_rename(path):
    head, fn = os.path.split(path)
    # atomic in most cases
    os.rename(path, os.path.join(head, "." + fn))


def parse_apps_installed(line):
    line_parts = line.strip().split(b'\t')
    if len(line_parts) < 5:
        return
    dev_type, dev_id, lat, lon, raw_apps = line_parts
    if not dev_type or not dev_id:
        return
    try:
        apps = [int(a.strip()) for a in raw_apps.split(b',')]
    except ValueError:
        apps = [int(a.strip()) for a in raw_apps.split(b',') if a.isidigit()]
        logging.info("Not all user apps are digits: `%s`" % line)
    try:
        lat, lon = float(lat), float(lon)
    except ValueError:
        logging.info("Invalid geo coords: `%s`" % line)
    return AppsInstalled(dev_type, dev_id, lat, lon, apps)


def process_data(filename: str, config):
    device_addr = {
        b'idfa': config.idfa,
        b'gaid': config.gaid,
        b'adid': config.adid,
        b'dvid': config.dvid,
    }

    processed = Value('i', 0)
    errors = Value('i', 0)

    threads = {}
    queues = {}
    buffer = {}
    for addr in device_addr.values():
        queue = Queue()
        queues[addr] = queue
        threads[addr] = MemcacheThread(addr, queue, processed, errors, config)
        threads[addr].start()
        buffer[addr] = []

    with gzip.open(filename) as fd:
        logging.info('Processing %s' % filename)
        for line in fd:
            line = line.strip()
            if not line:
                continue
            apps_installed = parse_apps_installed(line)
            if not apps_installed:
                errors += 1
                continue

            addr = device_addr.get(apps_installed.dev_type, None)
            if not addr:
                errors += 1
                logging.error("Unknown device type: %s (file: %s)"
                              % (apps_installed.dev_type, filename))
                continue

            buffer[addr].append(apps_installed)
            if len(buffer[addr]) >= config.buffer_size:
                queues[addr].put(buffer[addr])
                buffer[addr] = []

    for addr in device_addr.values():
        queues[addr].put(buffer[addr])
        queues[addr].put('quit')

    for thread in threads:
        threads[thread].join()

    err_rate = float(errors.value) / processed.value
    if err_rate < NORMAL_ERR_RATE:
        logging.info("Acceptable error rate (%s). Successful load" % err_rate)
    else:
        logging.error("High error rate (%s > %s). Failed load" % (err_rate, NORMAL_ERR_RATE))

    dot_rename(filename)


def main(config):
    try:
        files = tuple(sorted(glob.iglob(config.pattern)))
        pool = Pool(processes=config.processes)
        pool.map(partial(process_data, config=config), files)
    except Exception as exc:
        logging.exception("Unexpected error: %s" % exc)


def prototest():
    sample = "idfa\t1rfw452y52g2gq4g\t55.55\t42.42\t1423,43,567,3,7,23\n" \
             "gaid\t7rfw452y52g2gq4g\t55.55\t42.42\t7423,424"
    for line in sample.splitlines():
        dev_type, dev_id, lat, lon, raw_apps = line.strip().split("\t")
        apps = [int(a) for a in raw_apps.split(",") if a.isdigit()]
        lat, lon = float(lat), float(lon)
        ua = appsinstalled_pb2.UserApps()
        ua.lat = lat
        ua.lon = lon
        ua.apps.extend(apps)
        packed = ua.SerializeToString()
        unpacked = appsinstalled_pb2.UserApps()
        unpacked.ParseFromString(packed)
        assert ua == unpacked


if __name__ == '__main__':
    op = OptionParser()
    op.add_option("-p", "--processes", action="store", default=cpu_count())
    op.add_option("-w", "--workers", action="store", default=2)
    op.add_option("-b", "--buffer_size", action="store", default=100)

    op.add_option("-t", "--test", action="store_true", default=False)
    op.add_option("-l", "--log", action="store", default=None)
    op.add_option("--dry", action="store_true", default=False)
    op.add_option("--pattern", action="store", default="data/*.tsv.gz")
    op.add_option("--idfa", action="store", default="127.0.0.1:33013")
    op.add_option("--gaid", action="store", default="127.0.0.1:33014")
    op.add_option("--adid", action="store", default="127.0.0.1:33015")
    op.add_option("--dvid", action="store", default="127.0.0.1:33016")
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO if not opts.dry else logging.DEBUG,
                        format='[%(asctime)s] %(levelname).1s %(message)s',
                        datefmt='%Y.%m.%d %H:%M:%S')
    if opts.test:
        prototest()
        sys.exit(0)

    logging.info("Memcache loader started with options: %s" % opts)
    main(opts)
